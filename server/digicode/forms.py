# -*- coding: utf-8 -*-

from django.forms import ModelForm
from models import Acces

class AccesPonctuel(ModelForm):
    class Meta:
        model = Acces
        fields = ['user', 'start_time', 'end_time']

