# -*- coding: utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.http import HttpResponseForbidden
import calendar as calendar_module
import datetime
from models import Code, Local, Acces
from forms import AccesPonctuel

def home(request):
    return render(request, "home.html", {'liste_local' : Local.objects.all()})

@login_required
def liste(request):
    return render(request, "liste.html", {
        'liste': Code.objects.all().filter(proprietaire=request.user),
    })

@login_required
def liste_respo(request, pk):
    # 
    local = get_object_or_404(Local, pk=pk)
    # 
    accesses = Acces.objects.all().filter(local=local)
    mine = accesses.filter(user=request.user)
    responsable = bool(mine.filter(responsable=True))
    if not mine:
        return HttpResponseForbidden("forbidden")

    return render(request, "liste_respo.html", {
        'local': local,
        'accesses': accesses,
        'responsable': responsable,
    })

@login_required
def edit_access(request, pk):
    acc = get_object_or_404(Acces, pk=pk)
    # Ai-je le droit ?
    if not Acces.objects.all().filter(local=local, user=request.user):
        return HttpResponseForbidden("forbidden")
    return 

@login_required
def add_acces(request, pk):
    local = get_object_or_404(Local, pk=pk)
    # Ai-je le droit ?
    if not Acces.objects.all().filter(local=local, user=request.user):
        return HttpResponseForbidden("forbidden")
    if request.method == 'POST':
        form = AccesPonctuel(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.local = local
            obj.responsable = False
            obj.save()
            # TODO: message de succès
            return redirect('liste_respo', pk)
    else:
        form = AccesPonctuel()
    return render(request, 'add_acces.html', {
        'form': form,
        'local': local,
    })

    
@login_required
def calendar(request, month=None, year=None, local=None):
    today = datetime.date.today()
    month = month or today.month
    year = year or today.year
    if not month and not year:
        return HttpResponseForbidden()
    c = calendar_module.Calendar()
    calendr = c.monthdatescalendar(int(year),int(month))
    if not local :
        local = Local.objects.all()[0]	
    month_start = calendr[0][0]
    month_end = calendr[len(calendr)-1][6]
    reservations = Acces.objects.all().filter(start_time__lte = month_end,end_time__gte = month_start).order_by('start_time')
    final_calendar=[]
    for i in xrange(len(calendr)):
        final_calendar.append([])
        for j in xrange(7):
            final_calendar[i].append([calendr[i][j]])
            for reservation in reservations:
                 if reservation.start_time.date() <= calendr[i][j] <= reservation.end_time.date():  
                     final_calendar[i][j].append(reservation)
    return render(request, "calendar.html", {'calendar' : final_calendar})   
                     
    
