# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.urlresolvers import reverse

import random
import string
import datetime
# Create your models here.

def _generate_secret():
    """Génère un mot de passe aléatoire"""
    random.seed(datetime.datetime.now().microsecond)
    chars = string.letters + string.digits + '/=+*.,$'
    length = 255
    return u''.join([random.choice(chars) for _ in xrange(length)])

CODE_LENGTH=6

def _generate_code():
    """Génère un mot de passe aléatoire,
    TODO: en prendre un qui n'existe pas déjà"""
    random.seed(datetime.datetime.now().microsecond)
    chars = string.digits
    return u''.join([random.choice(chars) for _ in xrange(CODE_LENGTH)])

class Local(models.Model):
    """Un local (et sa badgeuse/digicode)"""

    class Meta:
        verbose_name_plural = "locaux"

    nom = models.CharField(max_length=255)

    description = models.TextField(blank=True)

    hostname = models.TextField(
        help_text=u"Nom de domaine ou IP de la badgeuse/digicode",
    )

    shared_secret = models.CharField(max_length=255,
        default=_generate_secret,
        help_text=u"Secret partagé stocké dans le digicode",
    )
    stockage = models.BooleanField(help_text=u"Si le local est pour du stockage")
    def __unicode__(self):
        return self.nom

    def try_code(self, code):
        """Essaie un code. Et le consomme si nécessaire (TODO)"""
        possibles = Code.objects.filter(touches=code).filter(locaux=self)
        if not possibles:
            return False
        else:
            for  possible in possibles:
                for acces in Acces.objects.filter(user=possible.proprietaire).filter(local=self):
                    if acces.start_time < timezone.now() and timezone.now() < acces.end_time :
                        log = Log()
                        log.user = acces.user
                        log.save()
                        return True
            return False

class Log(models.Model):
    """Un log pour une entrée dans un local """

    user = models.ForeignKey(User)

    date = models.DateTimeField(auto_now_add=True, blank=True)

    

class Acces(models.Model):
    """Un accès à un local donné"""
    
    local = models.ForeignKey(Local)
    
    user = models.ForeignKey(User)
    
    responsable = models.BooleanField(default = False,
        help_text=u"Est-ce le responsable de ce local (droits étendus)",
    )

    start_time = models.DateTimeField(null=True, blank=True,
        help_text=u"Date correspondant au début de la période de validité",
    )
    end_time = models.DateTimeField(null=True, blank=True,
        help_text=u"Date correspondant à la fin de la période de validité",
    )

    # TODO retirer ça/faire un truc mieux
    periodicity = models.IntegerField(default=0,
        help_text=u"Période en jour de répétition de la validité de l'accès",
    )

    # TODO: fonction statique qui prend un query dict 

    def details(self):
        if self.responsable:
            return u"Responsable"
        # TODO hebdomadaire
        if self.start_time and self.end_time:
            return u"de %s au %s" % (self.start_time, self.end_time)
    
    # Toute autre restriction d'accès envisageables (créneaux horaires etc)
    # Idem, mais avec des groupes (TODO  ?)
    def edit_url(self):
        return "/prout"
        return reverse('digicode.views.edit_acces', args=[str(self.id)])


class Code(models.Model):
    """Un code à quelqu'un.
    Tout utilisateur a le droit de créer un code, pour les locaux auxquels
    il a accès. Il peut ainsi définir si son code ne devra être utilisé qu'une
    fois, tout le temps, ou si il ne doit être valable que pour certains locaux.
    """
    
    touches = models.CharField(max_length=CODE_LENGTH,
        help_text=u"Une chaîne de caractère ne contenant que des chiffres",
        default=_generate_code,
        unique = True,
    )

    proprietaire = models.ForeignKey(User)
    
    locaux = models.ManyToManyField(Local,
        help_text=u"Locaux pouvant être ouverts par ce code",
    )

#    otp = models.BooleanField(default=False,
#        help_text=u"One-Time-Password : le code doit-il s'autodétruire après usage ?",
#    )
#
#    expire_time = models.DateTimeField(null=True, blank=True,
#        help_text=u"Date d'expiration",
#    )
