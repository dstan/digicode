#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# LOGIN.PY -- Gère l'interface d'authentification à l'aide des modèles Django.
#
# Copyright (C) 2009-2010 Nicolas Dandrimont
# Authors: Nicolas Dandrimont <olasd@crans.org>
# Censor: Antoine Durand-Gasselin <adg@crans.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.auth.models import Group, User
from django.contrib.auth.backends import ModelBackend
from django.utils.importlib import import_module
import socket
import ssl
import json
class NKError(Exception):
    pass
class NoteUserBackend(ModelBackend):
    """Authentifie un utilisateur à l'aide de la note"""

    supports_anonymous_user = False

    def authenticate(self, username=None, password=None):
        """Authentifie l'utilisateur sur la base LDAP. Crée un
        utilisateur django s'il n'existe pas encore."""

        if not username or not password:
            return None
        try:
            data = login_NK(username,password)            
        except NKError:
            return None
        
        # On stocke les utilisateurs dans la base avec un truc canonique
        print "%r" %data
        django_username = '#%s' % data['idbde']
        try:
            user = User.objects.get(username=django_username)
        except User.DoesNotExist:
            user = User(username=django_username, password="Note Backend User!")
        user.save()
        self.refresh_droits(user, data)
        self.refresh_fields(user, data)
        return user

    def refresh_droits(self, user, data):
        """Rafraîchit les droits de l'utilisateur django `user' depuis
        l'utilisateur LDAP `cl_user'"""
        if data['supreme']:
            user.is_staff = True
            user.is_superuser = True
        else:
            user.is_staff = False
            user.is_superuser = False

        groups = []
        """for cl_droit in cl_droits:
            group, created = Group.objects.get_or_create(name="crans_%s" % cl_droit.lower())
            group.save()
            groups.append(group)"""

        user.groups.add(*groups)
        user.save()

    def refresh_fields(self, user, data):
        """Rafraîchit les champs correspondants à l'utilisateur (nom,
        prénom, email)"""
        
        user.first_name = unicode(data['prenom'])
        user.last_name = unicode(data['nom'])
        mail = unicode(data['mail'])
        user.email = mail

        user.save()

    def get_user(self, uid):
        """Récupère l'objet django correspondant à l'uid"""
        try:
            return User.objects.get(pk=uid)
        except User.DoesNotExist:
            return None



def connect_NK():
    """Connecte une socket au servuer NK2015 et la renvoie après avoir effectué le hello.
       Lève une erreur en cas d'échec"""
    sock = socket.socket()
    try:
        # On établit la connexion sur port 4242
        sock.connect(("138.231.141.159", 4242))
        # On passe en SSL 
        # TODO ca a ajouté           
        sock = ssl.wrap_socket(sock, ca_certs=None)
        # On fait un hello
        sock.write(json.dumps(["hello", "HTTP Django"]))
        # On récupère la réponse du hello
        out = full_read(sock)
    except Exception as exc:
        print "Echec de connection %r" %exc
        # Si on a foiré quelque part, c'est que le serveur est down
        raise NKError()
    if out["retcode"] == 0:
        return sock
    else:
        print "Code d'erreur %s" %out["retcode"]
        raise NKError()

def full_read(socket):
    # On récupère d'abord la taille du message
    length_str = ''
    char = socket.recv(1)
    while char != '\n':
        length_str += char
        char = socket.recv(1)
    total = int(length_str)
    # On utilise une memoryview pour recevoir les données chunk par chunk efficacement
    view = memoryview(bytearray(total))
    next_offset = 0
    while total - next_offset > 0:
        recv_size = socket.recv_into(view[next_offset:], total - next_offset)
        next_offset += recv_size
    try:
        msg = json.loads(view.tobytes())
    except (TypeError, ValueError) as e:
        raise NKError()
    return msg

def login_NK(username, password):
    """Ouvre une connexion au serveur NK2015 par username/password
       Renvoie dans tous les cas un objet HttpResponse[Redirect] utilisable directement"""
    try:
        sock = connect_NK()
        data = [username, password, "bdd",[[],[],False]]
        paquet = ["login", data]
        sock.write(json.dumps(paquet))
        out = full_read(sock)
        retcode, errmsg = out["retcode"], out["errmsg"]
    except NKError as exc:
        raise NKError
    if retcode == 0:
        # login réussi
        # On demande au serveur qui on est
        sock.write(json.dumps(["whoami"]))
        out = full_read(sock)
        whoami = out["msg"]
        return whoami
    else:
        raise NKError


